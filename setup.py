#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from os import path

from setuptools import find_packages, setup

from wotpy.__version__ import __version__
from wotpy.support import (is_coap_supported, is_dnssd_supported,
                           is_mqtt_supported)

install_requires = [
    'tornado>=6.2,<7.0',
    'jsonschema>=4.17.3,<5.0',
    'reactivex>=4.0.4,<5.0',
    'python-slugify>=8.0.0,<9.0',
    'requests-oauthlib>=1.3.1,<1.4'
]

test_requires = [
    'pytest>=7.2.1',
    'pytest-cov>=4.0.0,<5.0.0',
    'pytest-rerunfailures>=11.1.1,<12.0',
    'faker>=17.0.0,<18.0.0',
    'Sphinx>=6.1.3,<7.0.0',
    'sphinx-rtd-theme>=1.2.0,<2.0.0',
    'pyOpenSSL>=23.0.0,<24.0.0',
    'coveralls>=3.3.1,<4.0',
    'coverage>=6.5.0,<7.0',
    'autopep8>=2.0.1,<3.0',
    'rope>=1.7.0,<2.0',
    'bump2version>=1.0,<2.0'
]

if is_coap_supported():
    install_requires.append('aiocoap[linkheader]==0.4.7')

if is_mqtt_supported():
    install_requires.append('amqtt==0.11.0b1')
    install_requires.append('websockets>=8.0')

if is_dnssd_supported():
    install_requires.append('zeroconf>=0.47.3,<0.57.0')
    test_requires.append('aiozeroconf==0.1.8')

this_dir = path.abspath(path.dirname(__file__))

with open(path.join(this_dir, 'README.md')) as fh:
    long_description = fh.read()

setup(
    name='wotpy',
    version=__version__,
    description='Python implementation of a W3C WoT Runtime and the WoT Scripting API',
    long_description=long_description,
    long_description_content_type='text/markdown',
    keywords='wot iot gateway fog w3c',
    author='Andres Garcia Mangas',
    author_email='andres.garcia@fundacionctic.org',
    url='https://github.com/agmangas/wot-py',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: 3.10',
        'Programming Language :: Python :: 3.11'
    ],
    packages=find_packages(),
    include_package_data=True,
    install_requires=install_requires,
    extras_require={
        'tests': test_requires,
        'uvloop': ['uvloop>=0.12.2,<0.13.0']
    }
)
