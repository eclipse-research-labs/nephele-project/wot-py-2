#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Default Servient class
"""

import ssl
import urllib.parse

from wotpy.protocols.http.client import HTTPClient
from wotpy.protocols.http.server import HTTPServer
from wotpy.protocols.coap.client import CoAPClient
from wotpy.protocols.coap.server import CoAPServer
from wotpy.protocols.mqtt.client import MQTTClient
from wotpy.protocols.mqtt.server import MQTTServer
from wotpy.protocols.ws.client import WebsocketClient
from wotpy.protocols.ws.server import WebsocketServer
from wotpy.wot.enums import SecuritySchemeType
from wotpy.wot.servient import Servient


class DefaultServient(Servient):
    """Servient with preconfigured values."""

    DEFAULT_CONFIG = {
        "servient": {
            "clientOnly": False
        },
        "http": {
            "port": 8080,
            "enabled": True,
            "security": {
                "scheme": SecuritySchemeType.NOSEC
            }
        },
        "coap": {
            "port": 5683,
            "enabled": True,
            "security": {
                "scheme": SecuritySchemeType.NOSEC
            }
        },
        "mqtt": {
            "enabled": False
        },
        "ws": {
            "port": 8081,
            "enabled": False
        }
    }

    def __init__(self, config):
        new_config = dict(self.DEFAULT_CONFIG)
        new_config.update(config)

        ssl_context = None
        if "serverCert" in new_config and "serverKey" in new_config:
            certfile = new_config["serverCert"]
            keyfile = new_config["serverKey"]

            ssl_context = ssl.create_default_context(ssl.Purpose.SERVER_AUTH)
            ssl_context.load_cert_chain(certfile=certfile, keyfile=keyfile)

        servers = []
        if not new_config["servient"]["clientOnly"]:
            if new_config["http"]["enabled"]:
                port = new_config["http"]["port"]
                security_scheme = new_config["http"]["security"]

                servers.append(HTTPServer(
                    port=port, security_scheme=security_scheme, ssl_context=ssl_context))

            if new_config["coap"]["enabled"]:
                port = new_config["coap"]["port"]
                security_scheme = new_config["coap"]["security"]

                servers.append(CoAPServer(port=port, security_scheme=security_scheme))

            if new_config["mqtt"]["enabled"]:
                broker_url = new_config["mqtt"]["brokerUrl"]

                if "username" in new_config["mqtt"] and "password" in new_config["mqtt"]:
                    username = new_config["mqtt"]["username"]
                    password = new_config["mqtt"]["password"]

                    url_parts = list(urllib.parse.urlparse(broker_url))
                    url_parts[1] = f"{username}:{password}@{url_parts[1]}"
                    broker_url = urllib.parse.urlunparse(url_parts)

                    servers.append(MQTTServer(broker_url))

            if new_config["ws"]["enabled"]:
                port = new_config["ws"]["port"]

                servers.append(WebsocketServer(port=port, ssl_context=ssl_context))

        clients = [
            HTTPClient(),
            CoAPClient(),
            MQTTClient(),
            WebsocketClient()
        ]

        super().__init__(clients=clients)

        for server in servers:
            self.add_server(server)

        if "credentials" in new_config:
            self.add_credentials(new_config["credentials"])
