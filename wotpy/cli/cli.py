#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
CLI for the WoT runtime that handles servient configuration and execution of WoT runtime scripts.
"""

import asyncio
import yaml
import argparse
import importlib.util

from wotpy.cli.default_servient import DefaultServient

async def run_script(script_path, config_path):
    """Creates a Servient based on the config file and runs the input script.
    The script must contain an async function named `main` that accepts the
    argument `wot`."""

    config = {}
    if config_path is not None:
        with open(config_path, "r") as config_file:
            config = yaml.safe_load(config_file)

    default_servient = DefaultServient(config)
    wot = await default_servient.start()

    spec = importlib.util.spec_from_file_location("wot_script", script_path)
    module = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(module)

    loop = asyncio.get_running_loop()
    loop.create_task(module.main(wot))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Run a WoT script optionally preconfigured by a config file.")
    parser.add_argument("script", help="input WoT script file")
    parser.add_argument("-f", "--config-file", help="path to the configuration file")
    args = parser.parse_args()

    loop = asyncio.get_event_loop()
    loop.create_task(run_script(args.script, args.config_file))
    loop.run_forever()
