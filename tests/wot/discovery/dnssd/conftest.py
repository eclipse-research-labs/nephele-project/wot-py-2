#!/usr/bin/env python
# -*- coding: utf-8 -*-

import asyncio
import collections
import logging
import socket

import pytest
from faker import Faker

from tests.utils import find_free_port
from wotpy.support import is_dnssd_supported
from wotpy.wot.servient import Servient

collect_ignore = []

if not is_dnssd_supported():
    logging.warning("Skipping DNS-SD tests due to unsupported platform")
    collect_ignore.extend(["test_service.py"])


@pytest.fixture
def asyncio_zeroconf():
    """Builds an aiozeroconf service instance and starts browsing for WoT Servient services.
    Provides a deque that contains the service state change history."""

    from aiozeroconf import Zeroconf, ServiceBrowser
    from wotpy.wot.discovery.dnssd.service import DNSSDDiscoveryService

    loop = asyncio.get_event_loop_policy().get_event_loop()

    service_history = collections.deque([])

    def on_change(zc, service_type, name, state_change):
        service_history.append((service_type, name, state_change))

    aio_zc = Zeroconf(loop, address_family=[socket.AF_INET])
    ServiceBrowser(aio_zc, DNSSDDiscoveryService.WOT_SERVICE_TYPE, handlers=[on_change])

    yield {
        "zeroconf": aio_zc,
        "service_history": service_history
    }

    async def close():
        await aio_zc.close()

    loop.run_until_complete(close())


@pytest.fixture
def dnssd_discovery():
    """Builds an instance of the DNS-SD service."""

    from wotpy.wot.discovery.dnssd.service import DNSSDDiscoveryService

    dnssd_discovery = DNSSDDiscoveryService()

    yield dnssd_discovery

    async def stop():
        await dnssd_discovery.stop()

    loop = asyncio.get_event_loop_policy().get_event_loop()
    loop.run_until_complete(stop())


@pytest.fixture
def dnssd_servient():
    """Builds a Servient with both the TD catalogue and the DNS-SD service enabled."""

    servient = Servient(
        catalogue_port=find_free_port(),
        dnssd_enabled=True,
        dnssd_instance_name=Faker().pystr())

    yield servient

    async def shutdown():
        await servient.shutdown()

    loop = asyncio.get_event_loop_policy().get_event_loop()
    loop.run_until_complete(shutdown())
